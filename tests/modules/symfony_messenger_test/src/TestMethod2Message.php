<?php

declare(strict_types = 1);

namespace Drupal\symfony_messenger_test;

/**
 * @see \Drupal\symfony_messenger_test\Messenger\TestMessageMethodHandler
 */
final class TestMethod2Message {

  /**
   * Creates a new TestMethod2Message.
   */
  public function __construct(
    public ?string $handledBy = NULL,
  ) {
  }

}
