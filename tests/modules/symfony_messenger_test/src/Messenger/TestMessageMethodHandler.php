<?php

declare(strict_types = 1);

namespace Drupal\symfony_messenger_test\Messenger;

use Drupal\symfony_messenger_test\TestMethod2Message;
use Drupal\symfony_messenger_test\TestMethodMessage;
use Symfony\Component\Messenger\Attribute\AsMessageHandler;

/**
 * Message handler method-tagged with #[AsMessageHandler].
 */
final class TestMessageMethodHandler {

  /**
   * Message handler for TestMethodMessage messages.
   */
  #[AsMessageHandler]
  public function fooBar(TestMethodMessage $message): void {
    $message->handledBy = __METHOD__;
  }

  /**
   * Message handler for TestMethod2Message messages.
   */
  #[AsMessageHandler]
  public function fooBar2(TestMethod2Message $message): void {
    $message->handledBy = __METHOD__;
  }

}
