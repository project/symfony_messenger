<?php

declare(strict_types = 1);

namespace Drupal\symfony_messenger_test\Messenger;

use Drupal\symfony_messenger_test\TestClassMessage;
use Symfony\Component\Messenger\Attribute\AsMessageHandler;

/**
 * Message handler class-tagged with #[AsMessageHandler].
 */
#[AsMessageHandler]
final class TestMessageClassHandler {

  /**
   * Message handler for TestClassMessage messages.
   */
  public function __invoke(TestClassMessage $message): void {
    $message->handledBy = __METHOD__;
  }

}
