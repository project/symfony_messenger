<?php

declare(strict_types = 1);

namespace Drupal\symfony_messenger_test;

/**
 * @see \Drupal\symfony_messenger_test\Messenger\TestMessageClassHandler
 */
final class TestClassMessage {

  /**
   * Creates a new TestClassMessage.
   */
  public function __construct(
    public ?string $handledBy = NULL,
  ) {
  }

}
