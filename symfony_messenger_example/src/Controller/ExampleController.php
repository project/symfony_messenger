<?php

declare(strict_types = 1);

namespace Drupal\symfony_messenger_example\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\symfony_messenger\ResponseTrait;
use Drupal\symfony_messenger_example\Command\ExampleRequest;
use Drupal\symfony_messenger_example\Command\ExampleResponse;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Messenger\MessageBusInterface;

/**
 * A controller that dispatches a message to the bus.
 */
class ExampleController extends ControllerBase {

  use ResponseTrait;

  /**
   * ExampleController constructor.
   *
   * @param \Symfony\Component\Messenger\MessageBusInterface $bus
   *   The message bus.
   */
  final public function __construct(
    private readonly MessageBusInterface $bus,
  ) {
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): ExampleController {
    return new static($container->get('symfony_messenger.bus.default'));
  }

  /**
   * Builds the response.
   *
   * @return array
   *   A render array.
   */
  public function build(): array {
    $envelope = $this->bus->dispatch(new ExampleRequest('Example title'));
    $result = $this->getHandlerResult($envelope);
    assert($result instanceof ExampleResponse);

    $build['content'] = [
      '#type' => 'item',
      '#markup' => $this->t('Request dispatched to the bus! Response was: @response', [
        '@response' => $result->getResponse(),
      ]),
    ];

    return $build;
  }

}
