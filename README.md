[[_TOC_]]

#### Introduction

Messenger module integrates the Symfony messenger component into Drupal.

#### Installation

You need to apply a [patch](https://www.drupal.org/node/2961380) to Drupal core to use this module, usually patches are
applied by Composer, see [here](http://www.yuseferi.com/en/blog/How-apply-patches-Drupal-8-Composer) for more
information.

Messenger must be downloaded with Composer, and can be installed like any other Drupal module.
