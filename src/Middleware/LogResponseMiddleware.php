<?php

declare(strict_types = 1);

namespace Drupal\symfony_messenger\Middleware;

use Drupal\symfony_messenger\HandlerResponseInterface;
use Drupal\symfony_messenger\ResponseTrait;
use Psr\Log\LoggerAwareTrait;
use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\Middleware\MiddlewareInterface;
use Symfony\Component\Messenger\Middleware\StackInterface;

/**
 * Log command responses.
 */
class LogResponseMiddleware implements MiddlewareInterface {

  use LoggerAwareTrait;
  use ResponseTrait;

  /**
   * Handle the envelope.
   *
   * @param \Symfony\Component\Messenger\Envelope $envelope
   *   The envelope.
   * @param \Symfony\Component\Messenger\Middleware\StackInterface $stack
   *   The stack.
   *
   * @return \Symfony\Component\Messenger\Envelope
   *   The envelope.
   */
  public function handle(Envelope $envelope, StackInterface $stack): Envelope {
    $result = $this->getHandlerResult($envelope);
    if ($result instanceof HandlerResponseInterface) {
      $this->logger->info('LogResponseMiddleware: ' . $result->getResponse());
    }

    return $stack->next()->handle($envelope, $stack);
  }

}
